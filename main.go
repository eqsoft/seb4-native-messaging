/*
 * The MIT License (MIT)
 *
 * Copyright © 2022 Stefan Schneider <schneider@hrz.uni-marburg.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package main

import (
	"flag"
	"log"
	"os"
	"path/filepath"

	host "github.com/rickypc/native-messaging-host"
)

const BIN_NAME = "snm"
const APP_NAME = "com.example.seb4.nativemessaging"
const EXT_ID = "chrome-extension://ndpojjcjpjgmdnbacclckpkfeolnidnj/"

var Log *log.Logger = Loggerx()

func main() {
	Log.Println(BIN_NAME + " : " + APP_NAME)
	defer os.Exit(0)

	messaging := (&host.Host{}).Init()
	// host.H is a shortcut to map[string]interface{}
	request := &host.H{}

	// Read message from os.Stdin to request.
	if err := messaging.OnMessage(os.Stdin, request); err != nil {
		Log.Fatalf("messaging.OnMessage error: %v", err)
	}

	// Log request.
	Log.Printf("request: %+v", request)
}

func Loggerx() *log.Logger {
	SNM_LOG_FILE_LOCATION := os.Getenv("SNM_LOG_FILE_LOCATION")
	if SNM_LOG_FILE_LOCATION == "" {
		cwd, _ := os.Getwd()
		SNM_LOG_FILE_LOCATION = filepath.Join(cwd, BIN_NAME+".log")
	} else {
		SNM_LOG_FILE_LOCATION = filepath.Join(SNM_LOG_FILE_LOCATION, BIN_NAME+".log")
	}
	flag.Parse()
	if _, err := os.Stat(SNM_LOG_FILE_LOCATION); os.IsNotExist(err) {
		file, err1 := os.Create(SNM_LOG_FILE_LOCATION)
		if err1 != nil {
			panic(err1)
		}
		return log.New(file, "", log.Ldate|log.Ltime|log.Lshortfile)
	} else {
		file, err := os.OpenFile(SNM_LOG_FILE_LOCATION, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
		if err != nil {
			panic(err)
		}
		return log.New(file, "", log.Ldate|log.Ltime|log.Lshortfile)
	}
}
