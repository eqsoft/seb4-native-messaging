module gitlab.com/eqsoft/seb4-native-messaging

go 1.20

require github.com/rickypc/native-messaging-host v1.16.0

require (
	github.com/hashicorp/go-version v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20210220050731-9a76102bfb43 // indirect
)
